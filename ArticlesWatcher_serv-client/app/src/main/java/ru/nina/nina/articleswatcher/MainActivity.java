package ru.nina.nina.articleswatcher;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String [] array;
    private Record [] recordsArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        addMyOwnListenerToElement();
        new JTask().execute();
    }

    private void printMessage(String s) {
        Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void setContentToElement(String s) {
        for (Record r : recordsArr) {
            if (r.article.equals(s)) {
                TextView t = (TextView) findViewById(R.id.textview_1);
                StringBuilder b = new StringBuilder();
                b.append("Тема:  ").append(r.tema).append("\n\n");
                b.append("Заголовок:  ").append(r.article).append("\n\n");
                b.append("Текст:  ").append(r.content).append("\n\n");
                t.setText(b.toString());
            }
        }
    }

    private void addMyOwnListenerToElement(){
        AdapterView.OnItemClickListener itemClickListener_1 = new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView <?> listView, View v, int position, long id){
                String s = ((TextView) v).getText().toString();
                printMessage(s);
                setContentToElement(s);
            }
        };
        ListView listView_1 = (ListView) findViewById(R.id.listview_1);
        listView_1.setOnItemClickListener(itemClickListener_1);
    }

    public void initListView(String  array[]) {
        ArrayAdapter <String> list_1_adapter = new ArrayAdapter <String> (this, android.R.layout.simple_list_item_1, array);
        ListView listView_1 = (ListView) findViewById(R.id.listview_1);
        listView_1.setAdapter(list_1_adapter);
    }

    public void getArticlesList(View view) {
        new JTask().execute();
    }

    private class JTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... voids) {
            String urlString = "https://api-news-nin1.herokuapp.com/";

            try {
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder buffer = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) buffer.append(line);

                ArrayList <String> arrList = new ArrayList <String>();
                ArrayList <Record> massList = new ArrayList <Record>();

                JSONArray arr = new JSONArray(buffer.toString());
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject element = arr.getJSONObject(i);
                    String tema = element.getString("tema");
                    String article = element.getString("article");
                    String content = element.getString("content");
                    arrList.add(article);
                    massList.add(new Record(tema, article, content));
                }

                int n = arrList.size();
                array = new String[n];
                recordsArr = new Record[n];

                int count = 0;
                for(int i = 0; i < n; i++) {
                    String s = arrList.get(i);
                    array[i] = s;

                    Record r = massList.get(i);
                    recordsArr[i] = new Record(r.tema, r.article, r.content);
                }
            } catch (Exception e) {
                // error
            }

            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            initListView(array);
        }
    }
}
