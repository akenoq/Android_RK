package ru.nina.nina.articleswatcher;

public class Record {
    public String tema = "";
    public String article = "";
    public String content = "";

    Record(String temaP, String articleP, String contentP) {
        tema = temaP;
        article = articleP;
        content = contentP;
    }
}
