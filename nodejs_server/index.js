/*   https://api-news-nin1.herokuapp.com/   */

"use strict";

let express = require("express");
let app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

let port = process.env.PORT || 5000;
app.listen(port);
console.log("Server works on port " + port);

const mass = [];

function addToArray(article, content, tema) {
    mass.push({
        article: article,
        content: content,
        tema: tema
    });
}

app.get('/*', (request, response) => {
    const url = request.url;
    const arr = url.split("/");
    const operation = arr[1];

    if(operation === "getOneObj") {
        const content = JSON.stringify({
            a: "aaaa",
            b: "bbbb",
            c: "cccc"
        });
        response.end(content);
        return;
    }

    const content = JSON.stringify(mass);
    response.end(content);
});

app.post('/*', (request, response) => {
    let dataString = "";
    request.on('data', (data) => {
        dataString += data;
    }).on('end', () => {

        let dataObj = null;
        let flag = true;

        try {
            dataObj = JSON.parse(dataString);
            flag = true;
        } catch (err) {
            flag = false;
        }

        if(flag === false) {
            response.end(JSON.stringify({
                message: "NOT_CORRECT_JSON"
            }));
            return;
        }

        if(dataObj.article === null || dataObj.content === null || dataObj.tema === null) {
            response.end(JSON.stringify({
                message: "NOT_ALL_FIELDS"
            }));
            return;
        }

        if(dataObj.article === undefined || dataObj.content === undefined || dataObj.tema === undefined) {
            response.end(JSON.stringify({
                message: "NOT_ALL_FIELDS"
            }));
            return;
        }

        let article = dataObj.article;
        let content = dataObj.content;
        let tema = dataObj.tema;

        addToArray(article, content, tema);

        response.end(JSON.stringify({
            message: "ADDING_RECORD_OK"
        }));

    });
});
